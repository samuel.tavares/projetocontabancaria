package br.com.bankline;

public class Conta {
    private int id;
    private Cliente titular;
    private double saldo;
    private double limiteDaConta;

    public Conta(int id, Cliente titular, double saldo, double limiteDaConta) {
        this.id = id;
        this.titular = titular;
        this.saldo = saldo;
        this.limiteDaConta = limiteDaConta;
    }

    public Conta(int id, Cliente titular, double saldo) {
        this.id = id;
        this.titular = titular;
        this.saldo = saldo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getLimiteDaConta() {
        return limiteDaConta;
    }

    public void setLimiteDaConta(double limiteDaConta) {
        this.limiteDaConta = limiteDaConta;
    }

    public void deposita(double valor, Conta conta){
        if (valor > 0){
            conta.saldo += valor;
        }
    }
    public boolean saca(double valor, Conta conta){
        if(conta.saldo+valor < 0){
            conta.saldo -= valor;
            return true;
        }
        return false;
    }
}
