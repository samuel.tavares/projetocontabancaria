package br.com.bankline;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    IO io = new IO();

        Map<String, String> dados = io.coletaDadosCliente();
	    Cliente cliente = new Cliente(
	            dados.get("cpf"),
                dados.get("nome"),
                Integer.parseInt(dados.get("idade"))
        );

        System.out.println(cliente.getNome());
        System.out.println(cliente.getCpf());
        System.out.println(cliente.getIdade());



    }
}
