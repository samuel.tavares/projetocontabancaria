package br.com.bankline;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {
    Scanner input = new Scanner(System.in);

    public static void imprimirMensagens(String msg){
        System.out.println(msg);
    }

    public String inputString(){
        String value = input.nextLine();
        return value;
    }

    public int inputInt(){
        int value = input.nextInt();
        return value;
    }

    public boolean verificaMaioridade(int idade){
        if (idade < 18){
            return true;
        }
        return false;
    }

    public Map<String, String> coletaDadosCliente(){
        imprimirMensagens("Digite o cpf do cliente: ");
        String cpf = inputString();
        imprimirMensagens("Digite o nome do cliente: ");
        String nome = inputString();
        imprimirMensagens("Digite a idade do cliente: ");
        Integer idade = inputInt();
        while(verificaMaioridade(idade)){
            imprimirMensagens("Usuário deve ter mais de 18 anos/" +
                    "Digite novamente a idade: ");
            idade = inputInt();
        }

        Map<String, String> dados = new HashMap<>();
        dados.put("cpf", cpf);
        dados.put("nome", nome);
        dados.put("idade", idade.toString());
        return dados;
    }
}
